const fs = require('fs')
const path = require('path')
const { spawnSync } = require('child_process')

const phase = process.argv[2]
if (!['pre', 'post'].includes(phase)) {
  console.error('First argument must be pre or post')
  process.exit(1)
}

const fname = path.join(__dirname, 'lerna.json')
const f = JSON.parse(fs.readFileSync(fname, 'utf-8'))

const add = 'projects/**'

fs.writeFileSync(
  fname,
  JSON.stringify({
    ...f,
    packages: f.packages
      .filter(p => p !== add)
      .concat(phase === 'pre' ? [add] : []),
  }, null, 2)+'\n',
)
