#!/bin/env node

const { runCommand } = require('./util')

const argv = process.argv.slice(2)
switch (argv[0]) {
  case 'tsc':
    runCommand(argv, ['typescript', 'tsc'])
    break
  case 'check':
    runCommand(argv, ['typescript', 'tsc'], ['--noEmit'])
    break
  case 'eslint':
    runCommand(argv, 'eslint')
    break
  case 'lint':
    runCommand(argv, 'eslint', ['src', '--ext', '.ts'])
    break
  default:
    console.error(`Unknown command "${argv[0]}"`)
    process.exitCode = 1
}
