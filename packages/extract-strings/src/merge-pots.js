const { po } = require('gettext-parser')
const fs = require('fs')
const path = require('path')
const minimist = require('minimist')

/* global __dirname */

const opts = minimist(process.argv.slice(2))

if (!opts.output || !opts._) {
  ;[
    'usage: merge-pots <pots> --output <filename>',
    'first .pot specified is used for headers',
  ].forEach((l) => console.log(l))
}

const files = opts._.map((f) => po.parse(fs.readFileSync(f, 'utf-8')))

function merge(a, b, path = '') {
  const merged = {}
  for (const [k, v] of Object.entries(a).concat(Object.entries(b))) {
    if (!(k in merged)) {
      merged[k] = v
    } else if (typeof v === 'object' && typeof merged[k] === 'object') {
      merged[k] = merge(merged[k], v, `${path}${path ? '.' : ''}${k}`)
    } else if (/\.comments$/.exec(path) && k === 'reference') {
      merged[k] = merged[k] + '\n' + v
    } else if (v === merged[k]) {
      // do nothing
    } else {
      throw new Error(
        `Error merging key ${path}.${k}. ${JSON.stringify(
          v,
        )} is not compatible with ${JSON.stringify(merged[k])}`,
      )
    }
  }
  return merged
}

const merged = po.compile({
  ...files[0],
  translations: files.map((f) => f.translations).reduce(merge),
})

fs.writeFileSync(opts['output'], merged, 'utf-8')
