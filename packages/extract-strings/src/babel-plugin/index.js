'use strict'

/* eslint-disable import/no-commonjs */
const utils = require('./utils')
const gettextParser = require('gettext-parser')
const fs = require('fs')
const path = require('path')
const { replaceTranslationsFromFile, headers } = require('./translation-file')

const DEFAULT_FUNCTION_NAMES = {
  gettext: ['msgid'],
  dgettext: ['domain', 'msgid'],
  ngettext: ['msgid', 'msgid_plural', 'count'],
  dngettext: ['domain', 'msgid', 'msgid_plural', 'count'],
  pgettext: ['msgctxt', 'msgid'],
  dpgettext: ['domain', 'msgctxt', 'msgid'],
  npgettext: ['msgctxt', 'msgid', 'msgid_plural', 'count'],
  dnpgettext: ['domain', 'msgctxt', 'msgid', 'msgid_plural', 'count'],
}

const DEFAULT_FILE_NAME = 'gettext.po'

function getTranslatorComment(node) {
  const comments = []
  ;(node.leadingComments || []).forEach((commentNode) => {
    const match = commentNode.value.match(/^\s*translators:\s*(.*?)\s*$/im)
    if (match) {
      comments.push(match[1])
    }
  })
  return comments.length > 0 ? comments.join('\n') : null
}

function doesHaveProperty(node, name) {
  if (!node || node.type !== 'ObjectExpression') return false
  for (const property of node.properties || node.node.properties) {
    if (
      property.type === 'ObjectProperty' &&
      property.key &&
      property.key.type === 'Identifier' &&
      property.key.name === name
    ) {
      return true
    }
  }
  return false
}

module.exports = () => {
  let currentPoFile
  let data
  const relocatedComments = {}

  function init(plugin, self) {
    const functionNames =
      (plugin.opts && plugin.opts.functionNames) || DEFAULT_FUNCTION_NAMES
    const typeTagNames = (plugin.opts && plugin.opts.typeTagNames) || false
    const poFile = (plugin.opts && plugin.opts.fileName) || DEFAULT_FILE_NAME
    let base = plugin.opts && plugin.opts.baseDirectory
    if (base) {
      base = `${base.match(/^(.*?)\/*$/)[1]}/`
    }

    if (poFile !== currentPoFile) {
      currentPoFile = poFile
    }

    let fn = path.resolve(self.file.opts.cwd, self.file.opts.filename)
    if (base && fn && fn.substr(0, base.length) === base) {
      fn = fn.substr(base.length)
    }

    return { poFile, functionNames, base, fn, typeTagNames }
  }

  /**
   * @type { Map<string, { tr: { msgid: string }, line: number }[]> }
   */
  const translations = new Map()

  /**
   * @param file {string}
   * @param line {number}
   * @param tr {{ msgid: string }}
   */
  function addTranslation(file, line, tr) {
    if (!translations.has(file)) translations.set(file, [])
    const list = translations.get(file)
    if (!list.some((el) => el.line === line && el.tr.msgid === tr.msgid))
      list.push({ tr, line })
  }
  return {
    visitor: {
      Program: {
        enter(p, plugin) {
          const { functionNames, base, fn, typeTagNames } = init(plugin, this)

          // traverse path separately to prevent this running after transforms
          // removing it
          if (typeTagNames) {
            p.traverse({
              TSTypeReference(nodePath) {
                const { node } = nodePath
                if (!node.typeName) return
                if (node.typeName.name !== 'TrTag') return
                const { params } = node.typeParameters
                if (!params.length) return
                if (params[0].type !== 'TSLiteralType') return
                const { literal } = params[0]

                addTranslation(fn, node.loc.start.line, {
                  msgid: literal.value,
                })
                //console.log(nodePath)
              },
            })
          }
        },
        exit(p, plugin) {
          const { poFile, functionNames, base, fn, typeTagNames } = init(
            plugin,
            this,
          )
          replaceTranslationsFromFile({
            poFile,
            filename: fn,
            translations: translations.get(fn) || [],
          })
        },
      },
      VariableDeclaration(nodePath) {
        const translatorComment = getTranslatorComment(nodePath.node)
        if (!translatorComment) {
          return
        }
        nodePath.node.declarations.forEach((declarator) => {
          const comment = getTranslatorComment(declarator)
          if (!comment) {
            const key = `${declarator.init.start}|${declarator.init.end}`
            relocatedComments[key] = translatorComment
          }
        })
      },

      CallExpression(nodePath, plugin) {
        const { functionNames, base, fn } = init(plugin, this)
        const nplurals = Number.parseInt(
          /nplurals ?= ?(\d)/.exec(headers['plural-forms'])[1],
          10,
        )

        const callee = nodePath.node.callee

        if (
          callee.name in functionNames ||
          (callee.property && callee.property.name in functionNames)
        ) {
          const functionName =
            functionNames[callee.name] || functionNames[callee.property.name]
          const translate = {}

          const args = nodePath.get('arguments')
          for (let i = 0, l = args.length; i < l; i += 1) {
            const name = functionName[i]

            if (name && name !== 'count' && name !== 'domain') {
              const arg = args[i].evaluate()
              let value = arg.value
              if (arg.confident && value) {
                if (plugin.opts.stripTemplateLiteralIndent) {
                  value = utils.stripIndent(value)
                }
                translate[name] = value
              }

              if (name === 'msgid_plural') {
                translate.msgstr = []
                for (let p = 0; p < nplurals; p += 1) {
                  translate.msgstr[p] = ''
                }
              }
            }
          }
          if (doesHaveProperty(args[1], 'count')) {
            translate.msgstr = Array(nplurals).fill('')
            if (!translate.msgid_plural) {
              translate.msgid_plural = translate.msgid
            }
          }

          translate.comments = {}
          let lineno = -1
          if (nodePath.node.loc) {
            lineno = nodePath.node.loc.start.line
          }

          let translatorComment = getTranslatorComment(nodePath.node)
          if (!translatorComment) {
            translatorComment = getTranslatorComment(nodePath.parentPath)
            if (!translatorComment) {
              translatorComment =
                relocatedComments[`${nodePath.node.start}|${nodePath.node.end}`]
            }
          }

          if (translatorComment) {
            translate.comments.translator = translatorComment
          }
          addTranslation(fn, lineno, translate)
        }
      },
    },
  }
}
