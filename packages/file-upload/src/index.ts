import latinize from 'latinize'
import multer from 'multer'
import crypto from 'crypto'
import mime from 'mime'
import Jimp from 'jimp'
import sanitizeFilename from 'sanitize-filename'
import { Client } from 'minio'
import { RequestHandler, Request } from 'express'
import { promisify } from 'util'

type FileOpts = {
  /**
   * defaults to false
   */
  imageOnly?: boolean
}

type MinDbFile<Group> = {
  token: string
  group: Group | null
  imageMetadata?: any
}
type Settings<Group, DbFile extends MinDbFile<Group>, Context> = {
  bucket: string
  minio: Client
  temporaryStorage?: multer.StorageEngine
  getContext: (req: Request) => Promise<Context>
  saveFile: (
    context: Context,
    file: {
      etag: string
      bucketPath: string
      originalFilename: string
      byteCount: number
      imageMetadata: null | {
        width: number
        height: number
        thumbnail: string
      }
      token: string
    },
  ) => Promise<DbFile>
  getFileByToken: (context: Context, token: string) => Promise<DbFile | null>
  getFileById: (context: Context, id: string) => Promise<DbFile | null>
  middlewareReturn: (files: DbFile[]) => any
  duplicateFile: (context: Context, file: DbFile) => Promise<DbFile>
  createFileGroup: (context: Context) => Promise<Group>
  setFileGroup: (
    context: Context,
    files: DbFile[],
    group: Group | null,
  ) => Promise<void>
  getFilesFromGroup: (context: Context, group: Group) => Promise<DbFile[]>
  isSameFile: (a: DbFile, b: DbFile) => boolean
}

function slugify(text: string) {
  return latinize(text)
    .toLowerCase()
    .replace(/[^a-z0-9]/g, ' ')
    .trim()
    .replace(/ /g, '-')
    .replace(/-+/g, '-')
}

function cleanupFilename(fname: string) {
  const parts = fname.split('.')
  if (parts.length === 3) {
    // potentially .tar.gz-like
    if (parts[1].length <= 3 && parts[2].length <= 3) {
      if (parts.map((p) => p.length).reduce((a, b) => a + b) + 2 <= 64) {
        return parts.map(slugify).join('.')
      }
    }
  }

  const ext = slugify(parts.splice(parts.length - 1)[0])
  const start = slugify(parts.join('.'))
  if (ext.length + start.length + 1 <= 64) {
    return `${start}.${ext}`
  }
  if (ext.length > 63) {
    return `${start}.${ext}`.substring(0, 64)
  }
  return `${start.substring(0, 64 - 1 - ext.length)}.${ext}`
}

function hashBuffer(buffer: Buffer) {
  return crypto
    .createHash('sha512')
    .update(buffer)
    .digest()
    .toString('base64')
    .replace(/=+$/, '')
    .replace(/\//g, '_')
    .replace(/\+/g, '-')
}

async function generateThumb(
  buffer: Buffer,
): Promise<{
  thumbnail: string
  width: number
  height: number
}> {
  const img = await Jimp.read(buffer)
  const thumb = await img.resize(10, 10)
  const thumbBuffer = await new Promise<Buffer>((res, rej) =>
    thumb.getBuffer(Jimp.MIME_PNG, (err, buf) => {
      if (err) rej(err)
      else res(buf)
    }),
  )
  return {
    thumbnail: thumbBuffer.toString('base64'),
    width: img.bitmap.width || 0,
    height: img.bitmap.height || 0,
  }
}

function isImage(type: string) {
  return ['image/jpeg', 'image/png', 'image/webp'].includes(type)
}

function upload<Group, DbFile extends MinDbFile<Group>, Context>(
  settings: Settings<Group, DbFile, Context>,
  {
    filename,
    buffer,
    type,
  }: { filename: string; buffer: Buffer; type: string },
) {
  return settings.minio.putObject(
    settings.bucket,
    filename,
    buffer,
    buffer.length,
    { type },
  )
}

type UploadedFile = {
  //fieldname: string // Field name specified in the form
  originalname: string // Name of the file on the user's computer
  //encoding: string // Encoding type of the file
  mimetype: string // Mime type of the file
  //size: string // Size of the file in bytes
  buffer: Buffer // A Buffer of the entire file	MemoryStorage
}

//https://tools.ietf.org/html/rfc4648#section-5
function base64Replace(v: string) {
  return v.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '')
}
async function randomToken() {
  // we add time to the beggining to make collision even less likely
  const timeBytes = []
  let calc = Math.floor(Date.now())
  while (calc) {
    timeBytes.push(calc % 256)
    calc = Math.floor(calc / 256)
  }
  const encodedTime = base64Replace(
    Buffer.from(timeBytes.reverse()).toString('base64'),
  )

  const bytes = await promisify(crypto.randomBytes)(42)
  return encodedTime + base64Replace(bytes.toString('base64'))
}

export async function handleFile<
  Group,
  DbFile extends MinDbFile<Group>,
  Context
>(
  file: UploadedFile,
  context: Context,
  settings: Settings<Group, DbFile, Context>,
) {
  const originalname = sanitizeFilename(file.originalname)
  const type = (mime as any).getType(originalname)
  const hash = hashBuffer(file.buffer)
  // start converting without slowing down other processing (no await)
  const thumb = isImage(file.mimetype) ? generateThumb(file.buffer) : null

  // upload to bucket
  const filename = `uploads/${hash}/${cleanupFilename(originalname)}`
  const etag = await upload(settings, { filename, buffer: file.buffer, type })
  const thumbnail = await thumb

  const token = `t:${await randomToken()}`

  return settings.saveFile(context, {
    etag,
    bucketPath: filename,
    originalFilename: originalname,
    byteCount: file.buffer.byteLength,
    imageMetadata: thumbnail
      ? {
          width: thumbnail.width,
          height: thumbnail.height,
          thumbnail: thumbnail.thumbnail,
        }
      : null,
    token,
  })
}

function combineMiddleware(mids: RequestHandler[]): RequestHandler {
  return mids.reduce((a, b) => (req, res, next) => {
    a(req, res, (err: any) => {
      if (err) {
        return next(err)
      }
      return b(req, res, next)
    })
  })
}

export function createFileUpload<
  Group,
  DbFile extends MinDbFile<Group>,
  Context
>(settings: Settings<Group, DbFile, Context>) {
  const uploader = multer({
    storage: settings.temporaryStorage || multer.memoryStorage(),
    limits: {
      // more than 20MB or 20MiB, this should be also enforced by frontend
      fileSize: 21 * 1000 * 1000,
      fields: 10,
      files: 10,
      parts: 20,
    },
  })

  function checkOpts(opts: FileOpts, file: DbFile) {
    if (opts.imageOnly && !file.imageMetadata) {
      throw new Error('This is not image')
    }
  }

  async function checkTokenAndGetFile(
    token: string,
    context: Context,
  ): Promise<{ file: DbFile; isToken: boolean }> {
    const isToken = token.startsWith('t:')
    const file = isToken
      ? await settings.getFileByToken(context, token)
      : await settings.getFileById(context, token)

    if (!file) throw new Error('Uploaded file was not found')

    return { file, isToken }
  }

  function checkAndGetFiles(
    tokens: string[],
    context: Context,
    opts: FileOpts,
  ) {
    return Promise.all(
      tokens.map(async (token) => {
        const file = await checkTokenAndGetFile(token, context)
        checkOpts(opts, file.file)
        return file
      }),
    )
  }

  async function mapFileArrayDuplicateIf<
    T extends {
      file: DbFile
    }
  >(
    infos: T[],
    predicate: (file: DbFile) => boolean,
    context: Context,
  ): Promise<T[]> {
    return Promise.all(
      infos.map(async (info) => {
        if (predicate(info.file)) {
          return {
            ...info,
            file: await settings.duplicateFile(context, info.file),
          }
        }
        return info
      }),
    )
  }

  async function createFileGroup(
    tokens: string[],
    context: Context,
    opts: FileOpts = {},
  ) {
    if (!Array.isArray(tokens)) {
      throw new Error('Tokens must be array; this is backend bug')
    }

    if (tokens.length === 0) return null
    const checkedFiles = await checkAndGetFiles(tokens, context, opts)
    for (const { isToken } of checkedFiles) {
      if (!isToken) {
        throw new Error('Only use tokens while creating file group')
      }
    }
    const files = await mapFileArrayDuplicateIf(
      checkedFiles,
      (f) => !!f.group,
      context,
    )

    const fileGroup = await settings.createFileGroup(context)
    await settings.setFileGroup(
      context,
      files.map((f) => f.file),
      fileGroup,
    )
    return fileGroup
  }

  async function fileGroupFromBuffer(buffer: Buffer, context: Context) {
    const file = await handleFile(
      {
        buffer,
        mimetype: 'application/pdf',
        originalname: 'invoice.pdf',
      },
      context,
      settings,
    )
    return createFileGroup([file.token], context)
  }

  async function updateFileGroup(
    group: Group,
    tokens: string[],
    context: Context,
    opts: FileOpts = {},
  ) {
    if (!Array.isArray(tokens)) {
      throw new Error('Tokens must be array')
    }

    const newFileInfos = await mapFileArrayDuplicateIf(
      await checkAndGetFiles(tokens, context, opts),
      (f) => !!(f.group && f.group !== group),
      context,
    )

    const originalFiles = await settings.getFilesFromGroup(context, group)

    for (const { file, isToken } of newFileInfos) {
      if (
        !isToken &&
        !originalFiles.find((f) => settings.isSameFile(f, file))
      ) {
        throw new Error('File ids can be only used for not changing files')
      }
    }
    const newFiles = newFileInfos.map((f) => f.file)

    // remove extras from group
    await settings.setFileGroup(
      context,
      originalFiles.filter(
        (f) => !newFiles.some((n) => settings.isSameFile(f, n)),
      ),
      null,
    )

    // add missing files
    await settings.setFileGroup(
      context,
      newFiles.filter(
        (n) => !originalFiles.some((f) => settings.isSameFile(f, n)),
      ),
      group,
    )

    return newFiles
  }

  async function updateOrCreateFileGroup(
    group: Group | null,
    tokens: string[],
    context: Context,
    opts: FileOpts = {},
  ) {
    if (group !== null) {
      await updateFileGroup(group, tokens, context, opts)
      return group
    }
    return createFileGroup(tokens, context, opts)
  }

  return {
    middleware: combineMiddleware([
      uploader.array('files'),
      async (req, res, next) => {
        try {
          const context = await settings.getContext(req)

          const files = Array.isArray(req.files) ? req.files : req.files.files
          const links = await Promise.all(
            files.map((f: UploadedFile) => handleFile(f, context, settings)),
          )
          res.setHeader('Content-Type', 'application/json')
          res.send(JSON.stringify(await settings.middlewareReturn(links)))
        } catch (e) {
          next(e)
        }
      },
    ]),
    updateOrCreateFileGroup,
    fileGroupFromBuffer,
    createFileGroup,
  }
}
