'use strict'

/* eslint-disable import/no-commonjs */

function getRef(v) {
  try {
    return v.comments.reference.toLowerCase()
  } catch {
    return null
  }
}

/*
 * Sorts the object keys by the file reference.
 * There's no guarantee of key iteration in order prior to es6
 * but in practice it tends to work out.
 */
function sortObjectKeysByRef(unordered) {
  const ordered = {}
  Object.keys(unordered)
    .sort((a, b) => {
      const refA = getRef(unordered[a])
      const refB = getRef(unordered[b])
      if (refA !== refB) {
        if (refA < refB) {
          return -1
        }
        return 1
      }

      // sort by keys if refs match
      if (a < b) return -1
      if (a > b) return 1
      return 0
    })
    .forEach((key) => {
      ordered[key] = unordered[key]
    })
  return ordered
}

function stripIndent(str) {
  if (str && str.replace && str.trim) {
    return str.replace(/(?:\n(?:\s*))+/g, ' ').trim()
  }
  return str
}

module.exports = {
  stripIndent,
  sortObjectKeysByRef,
}
