/* eslint-disable import/no-commonjs, no-console */

const babel = require('@babel/core')
const fs = require('fs')
const path = require('path')
const minimist = require('minimist')

function walkTree(dir, cb) {
  const contents = fs.readdirSync(dir)
  for (const entry of contents) {
    const fname = path.join(dir, entry)
    const stat = fs.statSync(fname)
    if (stat.isDirectory()) walkTree(fname, cb)
    else if (stat.isFile()) cb(fname)
    else console.log(`ignoring ${fname}`)
  }
}

function getExt(fname) {
  return `.${path.parse(fname).base.split('.').slice(1).join('.')}`
}

const opts = minimist(process.argv.slice(2))

if (opts['help'] || opts['h']) {
  ;[
    'extract-strings [options]',
    '--function-name    name of function which we extract (default: t)',
    '                   can be specified multiple times',
    '--type-tag-name    name of type tag which we extract (default: [])',
    '                   can be specified multiple times',
    '--output           name of output file (default: gettext.pot)',
    '--base-directory   directory to which we resolve file name comments (default: .)',
    '--ext              extensions of files to parse (default: .js, .ts, .jsx, .tsx)',
    '                   can be specified multiple times',
    '--src              directory with source files (default: src)',
    '--help, -h         prints this message',
  ].forEach((l) => console.log(l))
  process.exit(0)
}

function toArray(v) {
  if (Array.isArray(v)) return v
  return [v]
}

function fromEntries(iterable) {
  return [...iterable].reduce(
    (obj, { 0: key, 1: val }) => Object.assign(obj, { [key]: val }),
    {},
  )
}

const exts = opts.ext || ['.ts', '.tsx', '.js', '.jsx']
const pluginOptions = {
  functionNames: fromEntries(
    toArray(opts['function-name'] || ['t']).map((name) => [name, ['msgid']]),
  ),
  typeTagNames: fromEntries(
    toArray(opts['type-tag-name'] || []).map((name) => [name, ['msgid']]),
  ),
  stripIndent: false,
  fileName: opts['output'] || './gettext.pot',
  baseDirectory: path.resolve(process.cwd(), opts['base-directory'] || '.'),
}

try {
  fs.unlinkSync(pluginOptions.fileName)
} catch (e) {
  if (e.code !== 'ENOENT') throw e
}

walkTree(path.resolve(process.cwd(), opts.src || 'src'), (filename) => {
  if (!exts.includes(getExt(filename))) return

  const code = fs.readFileSync(filename, 'utf-8')
  const opts = babel.loadOptions({ filename })
  const ast = babel.parseSync(code, opts)
  babel.transformFromAstSync(ast, code, {
    ...opts,
    plugins: [[path.resolve(__dirname, 'babel-plugin'), pluginOptions]],
  })
})
