/* eslint-disable import/no-commonjs */
const path = require('path')
const fs = require('fs')
const { spawnSync } = require('child_process')

const run = args_ => {
  const args = typeof args_ === 'string' ? args_.split(' ') : args_
  const r = spawnSync(args[0], args.slice(1), {
    stdio: 'inherit',
  })
  if (r.error) {
    throw r.error
  }
  return r
}

function getPackageCommand(package, command) {
  const resolved = require.resolve(package)
  let comparison = package.split('/')
  comparison = comparison[comparison.length - 1]
  let dir = resolved.replace(/[^/]+\.js$/, '')
  while (path.basename(dir) !== comparison) {
    if (dir === '/') {
      throw new Error('Hit root')
    }
    dir = path.dirname(dir)
  }
  const packageJson = JSON.parse(
    fs.readFileSync(path.join(dir, 'package.json'), 'utf8'),
  )
  return path.join(dir, packageJson.bin[command || package])
}

module.exports.runCommand = function runCommand(argv, pkgcmd, args = []) {
  const [package, command] = Array.isArray(pkgcmd) ? pkgcmd : [pkgcmd]
  return run([
    process.argv[0],
    getPackageCommand(package, command),
    ...args,
    ...argv.slice(1),
  ])
}
