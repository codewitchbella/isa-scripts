/* eslint-disable import/no-commonjs */
const gettextParser = require('gettext-parser')
const fs = require('fs')
const utils = require('./utils')

const headers = {
  'content-type': 'text/plain; charset=UTF-8',
  'plural-forms': 'nplurals = 2; plural = (n !== 1);',
}
module.exports.headers = headers

module.exports.replaceTranslationsFromFile = replaceTranslationsFromFile
/**
 * @param p {{
 *   poFile: string
 *   filename: string
 *   translations: {
 *     tr: { [key: string]: any }
 *     line: number
 *   }[]
 * }}
 */
function replaceTranslationsFromFile({ poFile, filename, translations }) {
  const { data, prevText } = (() => {
    try {
      const prevText = fs.readFileSync(poFile, 'utf-8')
      const parsed = gettextParser.po.parse(prevText)
      // strip everything except translation database
      const context = parsed.translations['']
      delete context['']
      return {
        data: {
          charset: 'UTF-8',
          headers,
          translations: {
            context,
          },
        },
        prevText,
      }
    } catch (e) {
      if (e.code === 'ENOENT')
        return {
          data: {
            charset: 'UTF-8',
            headers,
            translations: { context: {} },
          },
          prevText: null,
        }
      throw e
    }
  })()

  if (!data.translations.context) data.translations.context = {}
  const context = data.translations.context
  // strip existing translations for this file
  for (const [key, value] of Object.entries(context || {})) {
    if (!value || !value.comments || !value.comments.reference) continue
    value.comments.reference = value.comments.reference
      .split('\n')
      .filter((r) => !r.startsWith(`${filename}:`))
      .join('\n')
  }

  // add actual translations
  for (const { tr, line } of translations) {
    if (context[tr.msgid] !== undefined) {
      context[tr.msgid] = {
        ...context[tr.msgid],
        ...tr,
        comments: {
          ...context[tr.msgid].comments,
          ...tr.comments,
          reference: context[tr.msgid].comments
            ? context[tr.msgid].comments.reference
                .split('\n')
                .concat(`${filename}:${line}`)
                .sort()
                .join('\n')
                .trim()
            : '',
        },
      }
    } else if (tr.msgid !== undefined) {
      context[tr.msgid] = {
        ...tr,
        comments: { ...tr.comments, reference: `${filename}:${line}` },
      }
    }
  }

  // Sort by file reference to make output idempotent for the same input.
  if (data.translations && data.translations.context) {
    data.translations.context = utils.sortObjectKeysByRef(
      data.translations.context,
    )
  }

  const newText = gettextParser.po.compile(data).toString('utf-8')
  if (newText !== prevText) {
    fs.writeFileSync(poFile, newText, 'utf-8')
  }
}
